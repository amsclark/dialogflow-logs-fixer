# Created by Alexander Clark of Clark Management Consulting
# Creative Commons CC0 v1.0 Universal Public Domain Dedication. No Rights Reserved.
# Version 0.1

import tkinter as tk
from tkinter import *
from tkinter import filedialog
from tkinter import ttk
import sys
import datetime
import json
import csv
import os



def browseFiles():
    filename = filedialog.askopenfilename(initialdir = "/",
                                          title = "Select a File",
                                          filetypes = [("Json", '*.json')])
    label_file_explorer.configure(text="File Opened: "+filename)

    with open(filename, 'r') as f:
        data = json.load(f)
    processLogs(data)

def save(cleanLog, headers):
    outputFileNameSuggest = "Cleaned_DialogFlow_Logs_" + datetime.datetime.now().strftime('%Y-%m-%d-%H-%M') + ".csv"
    savefilename = os.path.realpath(filedialog.asksaveasfile(mode='w', initialdir = "/", initialfile=outputFileNameSuggest, title = "Save As...", filetypes = [("csv", '*.csv')]).name)
    with open(savefilename, "w", newline="") as csv_output_file:
         dict_writer = csv.DictWriter(csv_output_file, headers)
         dict_writer.writeheader()
         dict_writer.writerows(cleanLog)
        


def processLogs(dirtyData):
    cleanLog = []
    for dirtyEntry in dirtyData:
        cleanEntry =    {
                            "ConversationTraceID": "",
                            "TimeStamp": "",
                            "ResponseOrRequest": "",
                            "IntentId": "",
                            "IntentName": "",
                            "UserSaid": "",
                            "ResolvedQuery": ""
                        }
        try:
            dirtyTextPayload = dirtyEntry['textPayload']
            if dirtyTextPayload.startswith('Dialogflow Request'):
                cleanEntry['ResponseOrRequest'] = "Request"
                dreqJsonString = dirtyTextPayload.split(' : ',1)[1]
                dreqJson = json.loads(json.loads(dreqJsonString)['query_input'])
                if 'text' in dreqJson:
                    user_said = dreqJson['text']['textInputs'][0]['text']
                    cleanEntry['UserSaid'] = user_said
            if dirtyTextPayload.startswith('Dialogflow Response'):
                # Why did Google decide to make textPayload contain valid JSON for Requests, but not for responses?
                cleanEntry['ResponseOrRequest'] = "Response"
                drespTrashString = dirtyTextPayload.split(' : ')[1]
                respLines = drespTrashString.splitlines()
                #keysToGrab = ['resolved_query', 'intent_id', 'intent_name']
                for respLine in respLines:
                    if 'resolved_query' in respLine:
                        cleanEntry['ResolvedQuery'] = respLine.split('"')[1]
                    elif 'intent_id' in respLine:
                        cleanEntry['IntentId'] = respLine.split('"')[1]
                    elif 'intent_name' in respLine:
                        cleanEntry['IntentName'] = respLine.split('"')[1]

            cleanEntry['ConversationTraceID'] = dirtyEntry['trace']
            cleanEntry["TimeStamp"] = dirtyEntry['timestamp']
        except KeyError:
            print("dirtyEntry did not contain a textPayload key. Skipping...")
            print(dirtyEntry)
        cleanLog.append(cleanEntry)
    headers = ['ConversationTraceID', 'TimeStamp', 'ResponseOrRequest', 'IntentId', 'IntentName', 'UserSaid', 'ResolvedQuery']
    save(cleanLog, headers)
    print("Writing csv spreadsheet file")
    print("Done.")

root = tk.Tk()
root.title("DialogFlow Logs Fixer")
root.geometry("700x200")

label_file_explorer = Label(root,
                            text = "Dialogflow Log Fixer",
                            width = 100, height = 4,
                            fg = "blue")

button_explore = Button(root,
                        text = "Open JSON Log File",
                        command = browseFiles)

button_exit = Button(root,
                     text = "Exit",
                     command = exit)

label_file_explorer.grid(column = 1, row = 1)

button_explore.grid(column = 1, row = 2)

button_exit.grid(column = 1, row = 3)

root.mainloop()

